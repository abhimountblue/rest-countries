import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function AreaSort({ sortAreaFunction, sortAreaValue }) {
  const isLight = useContext(ThemeContext)
  return (
    <div className={isLight ? "select-section" : "select-section-dark"}>
      <p>Area</p>
      <select
        name="filter-region"
        onChange={sortAreaFunction}
        value={sortAreaValue}
      >
        <option value="Sort" hidden>
          Sort
        </option>
        <option value="ascending">Ascending</option>
        <option value="descending">Descending</option>
      </select>
    </div>
  )
}

export default AreaSort
