import { Link } from "react-router-dom"
import { useContext } from "react"
import ThemeContext from "./DarkModeContext"
import leftArrow from "../assets/left-arrow.png"

function BackButton() {
  const isLight = useContext(ThemeContext)
  return (
    <Link to={"/"}>
      <button
        className="back-button"
        style={{
          background: isLight ? "#fff" : "#2b3743",
          color: isLight ? "black" : "#fff",
        }}
      >
        {
          <img
            src={leftArrow}
            alt={leftArrow}
            style={{
              height: "90%",
              filter: isLight ? "none" : "invert(100%)",
            }}
          />
        }
        Back
      </button>
    </Link>
  )
}

export default BackButton
