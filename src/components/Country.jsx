import { useContext } from "react"
import ThemeContext from "./DarkModeContext"
import { Link } from "react-router-dom"

function Country({ countryData }) {
  const isLight = useContext(ThemeContext)
  return (
    <div
        className="country-card"
        style={{
          background: isLight ? "#fff " : "#2B3743",
          color: isLight ? "black" : "#fff",
        }}
      >
        <Link to={`/country/${countryData.cca3}`}>
        <img
          src={countryData.flags.png}
          alt={countryData.name.common}
          className="country-image"
        />

        <div className="detail-section">
          <h2 className="country-name">{countryData.name.common}</h2>
          <p className="sub-detail">
            Population:{" "}
            <span className="dynamic-sub-detail">{countryData.population}</span>
          </p>
          <p className="sub-detail">
            Region:{" "}
            <span className="dynamic-sub-detail">{countryData.region}</span>
          </p>
          <p className="sub-detail">
            Capital:{" "}
            <span className="dynamic-sub-detail">
              {countryData.capital ? (
                <>{countryData.capital}</>
              ) : (
                "data not present"
              )}
            </span>
          </p>
          <p className="sub-detail">
            Area:{" "}
            <span className="dynamic-sub-detail">
              {countryData.area} sq. km
            </span>
          </p>
        </div>
    </Link>
      </div>
  )
}

export default Country
