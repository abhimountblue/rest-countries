import searchSvg from "../assets/search.svg"
import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function Search({ searchedFilterFunction, searchInput }) {
  const isLight = useContext(ThemeContext)

  return (
    <div className={isLight ? "search-section" : "search-section-dark"}>
      <img src={searchSvg} alt="search-icon" />
      <input
        type="text"
        placeholder="Search for a country..."
        onChange={searchedFilterFunction}
        value={searchInput}
      />
    </div>
  )
}

export default Search
