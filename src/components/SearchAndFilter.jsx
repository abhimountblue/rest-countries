import Search from "./Search"
import FilterRegion from "./FilterRegion"
import FilterSubRegion from "./FilterSubRegion"
import PopulationSort from "./PopulationSort"
import AreaSort from "./AreaSort"

function SearchAndFilter({
  region,
  filterFunction,
  searchedFilterFunction,
  searchInput,
  subFilterFunction,
  subregion,
  sortAreaFunction,
  sortPopulationFunction,
  selectedSubRegion,
  sortPopulationValue,
  sortAreaValue,
}) {
  return (
    <div className="search-filter">
      <Search
        searchedFilterFunction={searchedFilterFunction}
        searchInput={searchInput}
      />
      <PopulationSort
        sortPopulationFunction={sortPopulationFunction}
        sortPopulationValue={sortPopulationValue}
      />
      <AreaSort
        sortAreaFunction={sortAreaFunction}
        sortAreaValue={sortAreaValue}
      />
      <FilterRegion
        region={["all", ...region]}
        filterFunction={filterFunction}
      />
      <FilterSubRegion
        subregion={["all", ...subregion]}
        subFilterFunction={subFilterFunction}
        selectedSubRegion={selectedSubRegion}
      />
    </div>
  )
}

export default SearchAndFilter
