import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function NoCountryFound() {
  const isLight = useContext(ThemeContext)
  return (
    <div
      className="data-not-fount"
      style={{ color: isLight ? "blcak" : "#fff" }}
    >
      No such countries found
    </div>
  )
}

export default NoCountryFound
