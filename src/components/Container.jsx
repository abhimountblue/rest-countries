import Countries from "./Countries"
import SearchAndFilter from "./SearchAndFilter"
import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function Container({
  region,
  subregion,
  filterFunction,
  searchedFilterFunction,
  subFilterFunction,
  searchInput,
  sortPopulationFunction,
  sortAreaFunction,
  selectedSubRegion,
  sortPopulationValue,
  sortAreaValue,
  countryList,
}) {
  const isLight = useContext(ThemeContext)
  return (
    <div
      className="container"
      style={{ background: isLight ? "hsl(0, 0%, 98%)" : "#212D37" }}
    >
      <SearchAndFilter
        region={region}
        subregion={subregion}
        filterFunction={filterFunction}
        searchedFilterFunction={searchedFilterFunction}
        subFilterFunction={subFilterFunction}
        searchInput={searchInput}
        sortPopulationFunction={sortPopulationFunction}
        sortAreaFunction={sortAreaFunction}
        selectedSubRegion={selectedSubRegion}
        sortPopulationValue={sortPopulationValue}
        sortAreaValue={sortAreaValue}
      />
      <Countries countryList={countryList} />
    </div>
  )
}

export default Container
