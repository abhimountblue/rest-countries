import { useContext } from "react"
import ThemeContext from "./DarkModeContext"
import { Link } from "react-router-dom"
import BorderCountries from "./BorderCountries"

function CountryDetailPage({ countryData }) {
  const [country] = countryData
  const isLight = useContext(ThemeContext)
  return (
    <div className="country-page-detail-container">
      <div className="image-div">
        <img src={country.flags.svg} alt={country.flags.alt} />
      </div>
      <div
        className="content-section"
        style={{
          color: isLight ? "black" : "#fff",
        }}
      >
        <h2>{country.name.common}</h2>
        <div className="informations">
          <p className="points">
            Native name:{" "}
            {country.name.nativeName && (
              <span>
                {Object.values(country.name.nativeName)
                  .map((name) => name.common)
                  .join(", ")}
              </span>
            )}
          </p>
          <p className="points">
            Population: <span>{country.population}</span>
          </p>
          <p className="points">
            Region: <span>{country.region}</span>
          </p>
          <p className="points">
            Sub Region: <span>{country.subregion}</span>
          </p>
          {country.capital && (
            <p className="points">
              Capital: <span>{country.capital.join(",")}</span>
            </p>
          )}
          {country.tld && (
            <p className="points">
              Top Level Domain: <span>{country.tld.join(",")}</span>
            </p>
          )}

          {country.currencies && (
            <p className="points">
              Currencies:{" "}
              <span>
                {Object.values(country.currencies)
                  .map((currency) => currency.name)
                  .join(",")}
              </span>
            </p>
          )}
          {country.languages && (
            <p className="points">
              Languages:{" "}
              <span>{Object.values(country.languages).join(", ")} </span>
            </p>
          )}

          {country?.borders && country?.borders.length && (
            <p className="points border-countery">
              Border Countries:{" "}
              {country.borders.map((element) => {
                return <BorderCountries countryCode={element} key={element} />
              })}
            </p>
          )}
        </div>
      </div>
    </div>
  )
}

export default CountryDetailPage
