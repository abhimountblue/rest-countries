import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function Loader() {
  const isLight = useContext(ThemeContext)
  return (
    <div className="starting" style={{
      background: isLight? 'hsl(0, 0%, 98%)' :'#212D37'
    }}>
      <div className="loader"></div>
    </div>
  )
}

export default Loader