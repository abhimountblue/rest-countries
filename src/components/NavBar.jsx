import { useContext } from "react"
import DarkMode from "./DarkMode"
import ThemeContext from "./DarkModeContext"

function NavBar({ setLightFunction }) {
  const isLight = useContext(ThemeContext)
  return (
    <nav
      style={{
        background: isLight ? "#ffffff" : "#2B3843",
        color: isLight ? "black" : "#ffffff",
      }}
    >
      <h1>Where in the world?</h1>
      <DarkMode setLightFunction={setLightFunction} />
    </nav>
  )
}

export default NavBar
