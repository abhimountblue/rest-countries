import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function PopulationSort({ sortPopulationFunction, sortPopulationValue }) {
  const isLight = useContext(ThemeContext)
  return (
    <div className={isLight ? "select-section" : "select-section-dark"}>
      <p>Population</p>
      <select
        name="filter-region"
        onChange={sortPopulationFunction}
        value={sortPopulationValue}
      >
        <option value=""  hidden>
          Sort
        </option>
        <option value="ascending">Ascending</option>
        <option value="descending">Descending</option>
      </select>
    </div>
  )
}

export default PopulationSort
