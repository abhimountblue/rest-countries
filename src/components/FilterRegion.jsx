import { useContext } from "react"
import ThemeContext from "./DarkModeContext"
function FilterRegion({ region, filterFunction }) {
  const isLight = useContext(ThemeContext)

  return (
    <div className={isLight ?  "select-section":"select-section-dark"}>
      <p>Region</p>
      <select name="filter-region" onChange={filterFunction}>
        <option value="all" disabled  hidden>
          Filter
        </option>
        {region.map((element, index) => {
          return (
            <option key={index} value={element}>
              {element[0].toUpperCase() + element.slice(1)}
            </option>
          )
        })}
      </select>
    </div>
  )
}

export default FilterRegion
