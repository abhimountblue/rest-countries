import { useEffect, useState } from "react"
import Loader from "./Loader"
import Error from "./Error"
import Container from "./Container"

const RestApi = "https://restcountries.com/v3.1/all"
function HomePage() {
  const [countriesData, setCountriesData] = useState([])
  const [selectedRegion, setSelectedRegion] = useState("all")
  const [selectedSubRegion, setSelectedSubRegion] = useState("all")
  const [searchInput, setSearchInput] = useState("")
  const [sortPopulation, setSortPopulation] = useState("Sort")
  const [sortArea, setSortArea] = useState("Sort")
  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")

  let allRegion = []
  let allSubRegion = []

  useEffect(() => {
    fetch(RestApi)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        setCountriesData(data)
        setIsLoading(!isLoading)
      })
      .catch((err) => {
        setIsError(true)
        setErrorMessage(err.message)
        setIsLoading(!isLoading)
      })
  }, [])

  allRegion = findUniquevalue(countriesData, "region")
  allSubRegion = subRegionBasedOnRegion(selectedRegion, countriesData)

  function findUniquevalue(array, key) {
    const uniqueArray = []
    array.forEach((element) => {
      if (element[key]) {
        if (uniqueArray.includes(element[key]) == false) {
          uniqueArray.push(element[key])
        }
      }
    })
    return uniqueArray
  }

  function sortByPolution(event) {
    setSortArea("Sort")
    setSortPopulation(event.target.value)
  }

  function sortByArea(event) {
    setSortPopulation("Sort")
    setSortArea(event.target.value)
  }

  function filterByRegionHandler(event) {
    const value = event.target.value
    setSelectedSubRegion("all")
    setSelectedRegion(value)
  }

  function filterBySearchHandler(event) {
    const inputValue = event.target.value
    setSearchInput(inputValue)
  }

  function filterBySubRegionHangler(event) {
    const value = event.target.value
    setSelectedSubRegion(value)
  }

  function subRegionBasedOnRegion(region, data) {
    const subRegionOfRegion = []
    if (region === "all") {
      return findUniquevalue(data, "subregion")
    } else {
      data.forEach((country) => {
        if (country.region === region) {
          subRegionOfRegion.push(country)
        }
      })
      return findUniquevalue(subRegionOfRegion, "subregion")
    }
  }

  function sortByPopulatationAndArea() {
    let filterDataOfCountry = countriesData.filter((element) => {
      let searchedInput = false
      if (searchInput === "") {
        searchedInput = true
      } else if (
        element.name.common.toLowerCase().includes(searchInput.toLowerCase())
      ) {
        searchedInput = true
      }
      let region = false
      if (selectedRegion === "all") {
        region = true
      } else if (element.region === selectedRegion) {
        region = true
      }
      let subregion = false
      if (selectedSubRegion === "all") {
        subregion = true
      } else if (element.subregion === selectedSubRegion) {
        subregion = true
      }
      return searchedInput && region && subregion
    })

    if (sortArea !== "Sort") {
      if (sortArea === "ascending") {
        filterDataOfCountry.sort((countryA, countryB) => {
          return countryA.area - countryB.area
        })
      } else {
        filterDataOfCountry.sort((countryA, countryB) => {
          return countryB.area - countryA.area
        })
      }
    }
    if (sortPopulation !== "Sort") {
      if (sortPopulation === "ascending") {
        filterDataOfCountry.sort((countryA, countryB) => {
          return countryA.population - countryB.population
        })
      } else {
        filterDataOfCountry.sort((countryA, countryB) => {
          return countryB.population - countryA.population
        })
      }
    }
    return filterDataOfCountry
  }

  const filterDataOfCountry = sortByPopulatationAndArea()

  return (
    <>
        {isLoading ? (
          <Loader />
        ) : (
          <>
            {isError ? (
              <Error error={errorMessage} />
            ) : (
              <Container
                region={allRegion}
                subregion={allSubRegion}
                filterFunction={filterByRegionHandler}
                searchedFilterFunction={filterBySearchHandler}
                subFilterFunction={filterBySubRegionHangler}
                searchInput={searchInput}
                sortPopulationFunction={sortByPolution}
                sortAreaFunction={sortByArea}
                selectedSubRegion={selectedSubRegion}
                sortPopulationValue={sortPopulation}
                sortAreaValue={sortArea}
                countryList={filterDataOfCountry}
              />
            )}
          </>
        )}
    </>
  )
}

export default HomePage
