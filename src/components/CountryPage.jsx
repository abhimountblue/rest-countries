import { useEffect, useState, useContext } from "react"
import { useParams } from "react-router-dom"
import Loader from "./Loader"
import Error from "./Error"
import CountryDetailPage from "./CountryDetailPage"
import ThemeContext from "./DarkModeContext"
import BackButton from "./BackButton"

function CountryPage() {
  const { id } = useParams()
  const restAPI = `https://restcountries.com/v3.1/alpha/${id}`
  const [country, setCountry] = useState()
  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")
  const isLight = useContext(ThemeContext)

  useEffect(() => {
    setIsLoading(true)
    fetch(restAPI)
      .then((response) => {
        return response.json()
      })
      .then((countryData) => {
        if (countryData.status == "404") {
          setErrorMessage(countryData.message)
          setIsError(true)
          setIsLoading(false)
          return
        }
        setCountry(countryData)
        setIsLoading(false)
      })
      .catch((err) => {
        setErrorMessage(err.message)
        setIsError(true)
        setIsLoading(false)
      })
  }, [id])
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          {isError ? (
            <Error error={errorMessage} />
          ) : (
            <div
              className="country-page-container"
              style={{ background: isLight ? "hsl(0, 0%, 98%)" : "#212D37" }}
            >
              <BackButton />
              <CountryDetailPage countryData={country} />
            </div>
          )}
        </>
      )}
    </>
  )
}

export default CountryPage
