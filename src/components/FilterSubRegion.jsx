import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function FilterSubRegion({ subregion, subFilterFunction, selectedSubRegion }) {
  const isLight = useContext(ThemeContext)

  return (
    <div className={isLight ?"select-section": "select-section-dark"}>
      <p>Sub Region</p>
      <select
        name="filter-region"
        onChange={subFilterFunction}
        value={selectedSubRegion}
      >
        <option value="all" hidden>
          Filter
        </option>
        {subregion.map((element) => {
          return (
            <option key={element} value={element}>
              {element}
            </option>
          )
        })}
      </select>
    </div>
  )
}

export default FilterSubRegion
