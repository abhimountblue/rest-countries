import { Link } from "react-router-dom"
import { useContext } from "react"
import ThemeContext from "./DarkModeContext"


function BorderCountries({countryCode}) {
    const isLight = useContext(ThemeContext)
  return (
    <Link to={`/country/${countryCode}`}>
      <button
        className="country-button"
        style={{
          background: isLight ? "#fff" : "#2b3743",
          color: isLight ? "black" : "#fff",
        }}
      >
        {countryCode}
      </button>
    </Link>
  )
}

export default BorderCountries
