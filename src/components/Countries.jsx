import Country from "./Country"
import NoCountryFound from "./NoCountryFound"

function Countries({ countryList }) {
  return (
    <>
      {countryList.length === 0 ? (
        <NoCountryFound/>
      ) : (
        <div className="country-container">
          {countryList.map((element) => {
            return <Country key={element.name.common} countryData={element} />
          })}
        </div>
      )}
    </>
  )
}

export default Countries
