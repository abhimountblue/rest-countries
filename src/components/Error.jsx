import { useContext } from "react"
import ThemeContext from "./DarkModeContext"

function Error({ error }) {
  const isLight = useContext(ThemeContext)
  return (
    <div
      className="starting"
      style={{
        background: isLight ? "hsl(0, 0%, 98%)" : "#212D37",
      }}
    >
      <div className="error">
        {error}
      </div>
    </div>
  )
}

export default Error
