import { useState } from "react"
import NavBar from "./components/NavBar"
import "./App.css"
import ThemeContext from "./components/DarkModeContext"
import HomePage from "./components/HomePage"

import {BrowserRouter,Route,Routes} from 'react-router-dom'
import CountryPage from "./components/CountryPage"


function App() {
  const [isLight, setIsLight] = useState(true)
  return (
    <>
    <BrowserRouter>
      <ThemeContext.Provider value={isLight}>
          <>
            <NavBar setLightFunction={setIsLight} />
          </>
          <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/country/:id" element={<CountryPage/>}/>
          </Routes>
      </ThemeContext.Provider>
    </BrowserRouter>
    </>
  )
}

export default App
